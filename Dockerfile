FROM java:8
VOLUME /tmp
ADD build/libs/webapp-0.0.1-SNAPSHOT.jar webapp-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "webapp-0.0.1-SNAPSHOT.jar"]
