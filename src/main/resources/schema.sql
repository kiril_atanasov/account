DROP SCHEMA IF EXISTS `accs`;

CREATE SCHEMA `accs`;

use `accs`;


DROP TABLE IF EXISTS `account`;

CREATE TABLE account (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `date_of_birth` date,
  PRIMARY KEY (`id`)
);