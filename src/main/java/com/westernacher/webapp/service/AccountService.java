package com.westernacher.webapp.service;

import com.westernacher.webapp.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface AccountService {
    public List<Account> getAccounts(Sort sort);

    public Page<Account> getAPaginated(int pageIndex, int pageSize, Sort sort);

    public void saveAccount(Account account);

    public Account getAccount(long id);

    public void deleteAccount(long id);
}
