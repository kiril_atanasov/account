package com.westernacher.webapp.service;

import com.westernacher.webapp.entity.Account;
import com.westernacher.webapp.repository.AccountDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDAO accountDAO;

    @Override

    public List<Account> getAccounts(Sort sort) {
        List<Account> accounts = new ArrayList<>();
        Iterator<Account> iterator = accountDAO.findAll().iterator();
        while (iterator.hasNext()) {
            accounts.add(iterator.next());
        }
        return accounts;
    }

    @Override
    public Page<Account> getAPaginated(int pageIndex, int pageSize, Sort sort) {
        Pageable pageable = PageRequest.of(pageIndex -1, pageSize, sort);
        return accountDAO.findAll(pageable);
    }


    @Override
    public void saveAccount(Account account) {
        accountDAO.save(account);
    }

    @Override
    public Account getAccount(long id) {
        return accountDAO.findById(id).get();
    }

    @Override
    public void deleteAccount(long id) {
        accountDAO.deleteById(id);
    }
}
