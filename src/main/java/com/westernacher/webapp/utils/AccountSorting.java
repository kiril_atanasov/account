package com.westernacher.webapp.utils;

import org.springframework.data.domain.Sort;

public class AccountSorting {
    private static final String SORT_NAME_INPUT = "name";

    private static final String SORT_LAST_NAME_INPUT = "lastName";

    private static final String SORT_LATEST_INPUT= "latest";

    private static final Sort DEFAULT_SORT = Sort.by("id").ascending();

    public static Sort of(String parameter) {
        if (parameter == null || parameter.length() == 0) {
            return DEFAULT_SORT;
        }

        // special cases
        switch (parameter) {
            case SORT_LATEST_INPUT:
                return Sort.by("id").descending();

            case SORT_LAST_NAME_INPUT:
                return Sort.by("lastName", "firstName").ascending();

            case SORT_NAME_INPUT:
                return Sort.by("firstName", "lastName").ascending();
        }

        return Sort.by(parameter).ascending();
    }
}
