package com.westernacher.webapp.repository;

import com.westernacher.webapp.entity.Account;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccountDAO extends PagingAndSortingRepository<Account, Long> {
}
