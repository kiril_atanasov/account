package com.westernacher.webapp.controller;

import javax.validation.Valid;

import com.westernacher.webapp.entity.Account;
import com.westernacher.webapp.service.AccountService;
import com.westernacher.webapp.utils.AccountSorting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Account>> showAllAccounts(@RequestParam(value = "page", defaultValue = "1") int page, @RequestParam(value = "size", defaultValue = "5") int size, @RequestParam(value = "sort", defaultValue = "id") String sortType) {
        Sort sort = AccountSorting.of(sortType);
        Page<Account> accountsPage = accountService.getAPaginated(page, size, sort);

        if (page > accountsPage.getTotalPages()) {
            throw new RuntimeException("Resource not found exception");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("numberOfPages", Integer.toString(accountsPage.getTotalPages()));

        List<Account> accounts = accountsPage.getContent();

        return new ResponseEntity<>(accounts, headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Account> showEmployee(@PathVariable("id") long id) {
        Account account = accountService.getAccount(id);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Account> saveEmployee(@Valid @RequestBody Account account) {
        accountService.saveAccount(account);
        return new ResponseEntity<>(account, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Account> updateEmployee(@Valid @RequestBody Account account) {
        accountService.saveAccount(account);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") long id) {
        accountService.deleteAccount(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

/*    private String createHeaderLinks(PagedResources<Account> pr) {
        final StringBuilder linkHeader = new StringBuilder();
        linkHeader.append(buildHeaderLink(pr.))

        return linkHeader.toString();
    }

    private static String buildHeaderLink(final String uri, final String rel) {
        return "<" + uri + ">; rel=\"" + rel + "\"";
    }*/


}
